cmake_minimum_required(VERSION 3.5.1)
project(mission_pathcoverage)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_executable(mission_pathcoverage ${SOURCE_FILES}
        ../include/LatLong-UTM.cpp
        ../include/LatLong-UTM.h
        ../include/field.cpp)

include_directories(
        ../include
        ../include/fast-cpp-csv-parser)


find_package(Threads)
target_link_libraries(mission_pathcoverage ${CMAKE_THREAD_LIBS_INIT})


set(Boost_FOUND 1)
find_package(Boost 1.54.0 COMPONENTS system thread date_time filesystem REQUIRED)
if(Boost_FOUND)
    #message(WARNING "Boost_INCLUDE_DIRS: ${Boost_INCLUDE_DIRS}")
    #message(WARNING "Boost_LIBRARIES: ${Boost_LIBRARIES}")
    #message(WARNING "Boost_VERSION: ${Boost_VERSION}")
    include_directories(${Boost_INCLUDE_DIRS})
endif()

target_link_libraries(mission_pathcoverage ${Boost_LIBRARIES})


set(OpenCV_FOUND 1)
find_package( OpenCV REQUIRED )
target_link_libraries(mission_pathcoverage ${OpenCV_LIBS})