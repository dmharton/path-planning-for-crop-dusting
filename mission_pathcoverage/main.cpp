
#include <csv.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "GridMapper.cpp"
#include "LatLong-UTM.h"
#include "field.h"
#include "json.hpp"
#include <fstream>

using json = nlohmann::json;
using namespace std;

LatLongUTM latLongConverter;
GridMapper gm(1000, 1000);

string FIELD_NAME = "Soccer";
string PATTERN_NAME = "Racetrack";
string PATTERN_ITERATION = "3";


const double SWATH_WIDTH =  7.0;
const int GRID_SCALAR = 5;
const double PASS_HEIGHT =  10.0 + 18; // used to calculate stdDev of height
const double PASS_SPEED_MS =  11.0;


struct coordTime {
    double lat, lon, height;
    boost::posix_time::ptime timestamp;
};

struct utmCoord {
    double n, e, height;
    boost::posix_time::ptime timestamp;
};

struct waypointTime {
    int index;
    boost::posix_time::ptime timestamp;
    utmCoord utm;
};

struct speedTime {
    double speed;
    boost::posix_time::ptime timestamp;
};

int main(int argc, char* argv[]) {

    if(argc > 1) {
        FIELD_NAME = argv[1];
        PATTERN_NAME = argv[2];
        PATTERN_ITERATION = argv[3];
    }

    cout << argv[1] << argv[2] << argv[3] << endl;

    //get the field info
    Field field;
    string jsonString;
    field.loadFromMissionFile("/home/viki/ClionProjects/cropdusting/data/"+FIELD_NAME+"Field.mission", jsonString);

    point center = field.getCenter();
    utmCoord utmRef = {center.y, center.x};

    //draw field boundaries
    point prevPoint = field.points[3];
    for (point& p : field.points) {

        double x = (p.x - utmRef.e) * GRID_SCALAR,
                y = (p.y - utmRef.n) * GRID_SCALAR,
                prevX = (prevPoint.x - utmRef.e) * GRID_SCALAR,
                prevY = (prevPoint.y - utmRef.n) * GRID_SCALAR;


        gm.plotLine(x, y, prevX, prevY, 40, 0 , 100);
        prevPoint = p;
    }



    //load waypoints from mission file
    ifstream ifs("/home/viki/ClionProjects/cropdusting/data/"+FIELD_NAME+"_"+PATTERN_NAME+".mission");
    string content((istreambuf_iterator<char>(ifs)),
                   (istreambuf_iterator<char>()) );

    if (ifs.fail()) {
        cout << "Failed to load mission file!" << std::endl;
    }

    json mission = json::parse(content);
    vector<utmCoord> missionWaypoints;
    for (json& item : mission["items"]) {
        //if (item["command"] == 16) { // 16 means waypoint
            double coordN, coordE;
            int zone;

            LatLongUTM latLongConverter;
            latLongConverter.LLtoUTM(eWGS84, item["coordinate"][0], item["coordinate"][1], coordN, coordE, zone);

            missionWaypoints.push_back({coordN, coordE});
        //}
    }


    //draw the waypoint paths
    utmCoord prevM = missionWaypoints[0];
    for (utmCoord& m : missionWaypoints) {

        if (field.isPointInField({ m.e, m.n }) ||
            field.isPointInField({ prevM.e, prevM.n })) {

            double x = (m.e - utmRef.e) * GRID_SCALAR,
                    y = (m.n - utmRef.n) * GRID_SCALAR,
                    prevX = (prevM.e - utmRef.e) * GRID_SCALAR,
                    prevY = (prevM.n - utmRef.n) * GRID_SCALAR;

            gm.plotLine(x, y, prevX, prevY, 200, 200, 0);

        }
        prevM = m;
    }


    //get a record of passes from start to end of each pass
    vector<array<utmCoord, 2>> passes;
    array<utmCoord, 2> pass;
    prevM = missionWaypoints[0];
    for (utmCoord& m : missionWaypoints) {

        if (field.isPointInField({ m.e, m.n }) &&
            !field.isPointInField({ prevM.e, prevM.n })) {

            pass[0] = prevM;

        } else if (!field.isPointInField({ m.e, m.n }) &&
                   field.isPointInField({ prevM.e, prevM.n })) {
            pass[1] = m;
            passes.push_back(pass);
        }
        prevM = m;
    }




    // read flight data from csv
    io::CSVReader<14> in("/home/viki/ClionProjects/cropdusting/data/"+FIELD_NAME+"_"+PATTERN_NAME+PATTERN_ITERATION+".csv");

    in.read_header(io::ignore_extra_column, "timestamp", "message_type",
                   "val1", "val2", "val3", "val4", "val5", "val6", "val7", "val8", "val9", "val10", "val11", "val12");

    std::string timestamp, message_type,
            val1, val2, val3, val4, val5, val6, val7, val8, val9, val10, val11, val12;

    vector<waypointTime> waypointTimes;
    vector<coordTime> coordTimes;
    vector<utmCoord> utmCoords;
    vector<speedTime> speedTimes;
    vector<double> inFieldHeights;


    bool inField = false;
    int currentWaypoint = 0;
    while(in.read_row(timestamp, message_type,
                      val1, val2, val3, val4, val5, val6, val7, val8, val9, val10, val11, val12)){


        // get waypoint transition times
        if (message_type == "mavlink_mission_current_t") {

            int wpIndex = boost::lexical_cast<int>(val2);
            if (currentWaypoint != wpIndex) {
                waypointTimes.push_back(
                        {
                                wpIndex,
                                boost::date_time::parse_delimited_time<boost::posix_time::ptime>(timestamp, 'T'),
                                missionWaypoints[wpIndex-1]
                        }
                );
                currentWaypoint = wpIndex;

                //cout << wpIndex << " - " << timestamp << endl;
            }

        } else if (message_type == "mavlink_global_position_int_t") {

            // val4: Latitude, expressed as degrees * 1E7
            // val6: Longitude, expressed as degrees * 1E7
            // val8: Altitude in meters, expressed as * 1000 (millimeters), AMSL (not WGS84 - note that virtually all GPS modules provide the AMSL as well)
            // val10 Relative altitude above the home position in meters, expressed as * 1000 (millimeters)


            double lat, lon, height;
            lat = boost::lexical_cast<double>(val4)/10000000; //where did the decimal go?!
            lon = boost::lexical_cast<double>(val6)/10000000;
            height = 0;

            coordTimes.push_back(
                    {
                            lat, lon, height,
                            boost::date_time::parse_delimited_time<boost::posix_time::ptime>(timestamp, 'T')
                    });

            double N, E;
            int zone;
            latLongConverter.LLtoUTM(eWGS84, lat, lon, N, E, zone);
            utmCoords.push_back(
                    {
                            N, E, 0,
                            boost::date_time::parse_delimited_time<boost::posix_time::ptime>(timestamp, 'T')
                    });

            if (field.isPointInField({E, N})) {
                inField = true;

                inFieldHeights.push_back(
                        boost::lexical_cast<double>(val10)/1000 - PASS_HEIGHT
                );
            }


        } else if (message_type == "mavlink_vfr_hud_t") {

            if (inField) {
                speedTimes.push_back(
                        {
                                boost::lexical_cast<double>(val4) - PASS_SPEED_MS, //Current ground speed in m/s
                                boost::date_time::parse_delimited_time<boost::posix_time::ptime>(timestamp, 'T')
                        });
            }
        }

    }





    //draw the recorded flight path


    int i = 3;
    waypointTime  prevWP = waypointTimes[i],
                  currWP = waypointTimes[i];
    utmCoord prevCoord;
    vector<double> pathDeviations;
    utmCoord wpLine[2];
    int passCount = 0;
    utmCoord firstCoord = {0, 0, -1};
    utmCoord lastCoord;

    for (utmCoord& coord : utmCoords){

        if (coord.timestamp > currWP.timestamp && i < waypointTimes.size()){
            while (coord.timestamp > currWP.timestamp && i < waypointTimes.size()) {
                currWP = waypointTimes[i];
                i++;
                //cout << "currWP.timestamp - " << currWP.timestamp << endl;
                //cout << "coord.timestamp - " << coord.timestamp << endl;
            }

            wpLine[0] = waypointTimes[i-1].utm;
            wpLine[1] = waypointTimes[i-3].utm;
            //wpLine[0] = prevWP.utm;
            //wpLine[1] = currWP.utm;

            double e1 = (wpLine[0].e - utmRef.e) * GRID_SCALAR;
            double n1 = (wpLine[0].n - utmRef.n) * GRID_SCALAR;
            double e2 = (wpLine[1].e - utmRef.e) * GRID_SCALAR;
            double n2 = (wpLine[1].n - utmRef.n) * GRID_SCALAR;

            prevWP = currWP;
        }


        if (field.isPointInField( {coord.e, coord.n} )) {

            double relE = (coord.e - utmRef.e) * GRID_SCALAR;
            double relN = (coord.n - utmRef.n) * GRID_SCALAR;


            if (firstCoord.height == -1) {
                firstCoord = coord;
                //cv::waitKey(5000);
            }
            lastCoord = coord;


            if (field.isPointInField( {prevCoord.e, prevCoord.n} )) {
                double prevE = (prevCoord.e - utmRef.e) * GRID_SCALAR;
                double prevN = (prevCoord.n - utmRef.n) * GRID_SCALAR;

                gm.plot(relE, relN, 255);
                gm.drawFlighPath(relE, relN, prevE, prevN, SWATH_WIDTH * GRID_SCALAR);

            } else {
                //cout << "Pass Start" << endl;

                double e1 = (passes[passCount][0].e - utmRef.e) * GRID_SCALAR;
                double n1 = (passes[passCount][0].n - utmRef.n) * GRID_SCALAR;
                double e2 = (passes[passCount][1].e - utmRef.e) * GRID_SCALAR;
                double n2 = (passes[passCount][1].n - utmRef.n) * GRID_SCALAR;

                //gm.plotLine(e1, n1, e2, n2, 150);

                //gm.finishPass();
            }

            double distFromLine = gm.getDistanceFromPointToLine(
                    {passes[passCount][0].e, passes[passCount][0].n },
                    {passes[passCount][1].e, passes[passCount][1].n },
                    {coord.e, coord.n });

            //cout << "distFromLine: " << distFromLine << endl;
            pathDeviations.push_back(distFromLine);
            gm.show();

            cv::waitKey(1);

        } else if (field.isPointInField( {prevCoord.e, prevCoord.n} )) {
            //cout << "Pass Finish" << endl;
            gm.finishPass();
            passCount++;
        }


        prevCoord = coord;
    }
    gm.show();
    cv::waitKey(100);

    //get weighted average and stdDev of speed.

    double speedSum, speedMean, speedStdDev = 0.0;

    for (speedTime& st : speedTimes) {
        speedSum += st.speed;
    }
    speedMean = speedSum / speedTimes.size();
    for (speedTime& st : speedTimes) {
        speedStdDev += pow(st.speed - speedMean, 2);
    }
    speedStdDev = sqrt(speedStdDev / speedTimes.size());

    cout << "speedDeviations: \taverage\tstdDev" << endl;
    cout  << speedMean << "\t" << speedStdDev << endl << endl;
    //cout << speedStdDev << endl << endl;


    //get weighted average and stdDev of height.

    double heightSum, heightMean, heightStdDev = 0.0;

    for (double& h : inFieldHeights) {
        heightSum += h;
    }
    heightMean = heightSum / inFieldHeights.size();
    for (double& h : inFieldHeights) {
        heightStdDev += pow(h - heightMean, 2);
    }
    heightStdDev = sqrt(heightStdDev / inFieldHeights.size());

    cout << "heightDeviations: \taverage\tstdDev" << endl;
    //cout  << heightMean << "\t" << heightStdDev << endl << endl;
    cout << speedStdDev << "\t" << heightStdDev << endl << endl;
    //cout  << heightStdDev << endl << endl;


    double  x = (field.getCenter().x - utmRef.e) * GRID_SCALAR,
            y = (field.getCenter().y - utmRef.n) * GRID_SCALAR,
            angle = (atan2(field.points[0].y - field.points[1].y, field.points[0].x - field.points[1].x) * 180) / M_PI,
            mean, stDev;

    gm.getMeanStandardDeviationOfField(x, y,
                                       field.width * GRID_SCALAR,
                                       field.height * GRID_SCALAR,
                                       angle, mean, stDev);

    //cout << "Coverage: mean\tstdDev\tcoefficientOfVariation" << endl;
    //cout  << mean << "\t" << stDev << "\t" << stDev/mean << endl << endl;

    cout << "Field width, height: \n" << field.width << "\t" << field.height << endl;

    //get mean, standard deviation of flight path error

    double pathMin = *min_element(pathDeviations.begin(), pathDeviations.end());
    double pathMax = *max_element(pathDeviations.begin(), pathDeviations.end());

    double pathSum, pathMean, pathStdDev = 0.0;

    for (double p : pathDeviations) {
        pathSum += p;
    }
    pathMean = pathSum / pathDeviations.size();
    for (double p : pathDeviations) {
        pathStdDev += pow(p - pathMean, 2);
    }
    pathStdDev = sqrt(pathStdDev / pathDeviations.size());

    //cout << "pathDeviations: min\tmax\taverage\tstdDev" << endl;
    //cout  << pathMin << "\t" << pathMax << "\t" << pathMean << "\t" << pathStdDev << endl << endl;


    //cout << "StartTime: " << firstCoord.timestamp << endl;
    //cout << "EndTime: " << lastCoord.timestamp << endl;
    //cout << "ElapsedTime: " << endl << lastCoord.timestamp - firstCoord.timestamp << endl<<endl;

    //cv::waitKey(5000);
    gm.saveSnapshot("/home/viki/ClionProjects/cropdusting/output/"+FIELD_NAME+"_"+PATTERN_NAME+PATTERN_ITERATION+".png");
    cout << "Saved " << FIELD_NAME+"_"+PATTERN_NAME+PATTERN_ITERATION+".png" << endl;

    return 0;
}