#include <iostream>
#include <string>
//#include <curl/curl.h>
#include "HTTPDownloader.h"
#include "json.hpp"


using namespace std;
using json = nlohmann::json;

string data; //will hold the url's contents

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ //callback must have this declaration
    //buf is a pointer to the data that curl has for us
    //size*nmemb is the size of the buffer

    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

int main(int argc, char* argv[])
{
    float windMin, windMax, temperature, humidity;
    bool precip = false;

    if (argv[1] == "input") {



        cout << "windMin in mph" << endl;
        cin >> windMin;

        cout << "windMax in mph" << endl;
        cin >> windMax;

        if (windMax < windMin) {
            cout << "windMax cannot be less than windMin." << endl;
            return 0;
        }

        cout << "temperature in F" << endl;
        cin >> temperature;

        cout << "relative humidity" << endl;
        cin >> humidity;

        string precipCin;
        cout << "Is it precipitating? y or n" << endl;
        cin >> precipCin;
        precip = precipCin == "y";

    } else {

        string url = "http://api.openweathermap.org/data/2.5/weather?lat=47.45522512976472&lon=-122.29793808557952&appid=4c3708a2d96ad2f794053f089484a410";

        HTTPDownloader downloader;
        string content = downloader.download(url);
        cout << content << endl;

        json j = json::parse(content);

        //https://openweathermap.org/weather-conditions
        // list of weather id codes.
        //float windMin, windMax, temperature, humidity;
        //bool precip;

        cout << "Got weather conditions for " << j["name"] << endl;

        windMin = windMax = (float)j["wind"]["speed"] * 2.23694; //convert from Meters/second to Miles/hour
        cout << "Wind speed: " << windMax << endl;

        temperature = (float)j["main"]["temp"] * 9/5 - 459.67; //convert from Kelvin to Fahrenheit.
        cout << "Temperature: " << temperature << endl;

        humidity = j["main"]["humidity"];
        cout << "Humidity: " << humidity << endl;

        vector<int> weatherCodes;

        for (json& w : j["weather"]) {
            int id = w["id"];
            int idGroup = id / 100 % 10;

            if (idGroup == 2 //Thunderstorm
                || idGroup == 3 //Drizzle
                || idGroup == 5 //Rain
                || idGroup == 6 //Snow
                || idGroup == 7 //Atmosphere (mist, smoke, volcanic ash...)
                || (id >= 900 && id <= 906)) //Extreme (tornado, hurricane)
            {
                precip = true;
                cout << "Current condition: " << w["description"] << endl;
            }
        }
    }


    //standards from http://www.omafra.gov.on.ca/english/crops/facts/09-037w.htm

    bool fly = true;

    if (temperature > 77 || humidity < 40) {
        fly = false;
        cout << "Temperature and humidity is out of bounds." << endl;
    }

    if (precip) {
        cout << "Precipitation is present." << endl;
    }

    if (windMin < 1.24) {
        fly = false;
        cout << "Wind speed is too slow. There's a good change of temperature inversion." << endl;
    }

    if (windMax > 6.2) {
        fly = false;
        cout << "Wind speed is too high. High risk of drift." << endl;
    }


    cout << (fly ? "Conditions are good to fly." : "Conditions are not recommended for flying.") << endl;

    //CURL curl = curl_easy_init();
    //CURLcode result;
    //curl = curl_easy_init();
    //return 0;


    //CURL* curl; //our curl object

    //curl_global_init(CURL_GLOBAL_ALL); //pretty obvious
    //curl = curl_easy_init();

    //auto url = "http://api.openweathermap.org/data/2.5/weather?lat=47.45522512976472&lon=-122.29793808557952&appid=4c3708a2d96ad2f794053f089484a410";

    //curl_easy_setopt(curl, CURLOPT_URL, "http://api.openweathermap.org/data/2.5/weather?lat=47.45522512976472&lon=-122.29793808557952&appid=4c3708a2d96ad2f794053f089484a410");
    //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    //curl_easy_perform(curl);

    //cout << endl << data << endl;
    //cin.get();

    //curl_easy_cleanup(curl);
    //curl_global_cleanup();

    //return 0;
}