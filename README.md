# Crop Dusting Utilities #


This project contains a series of tools useful for aerial application (crop dusting). It contains three main modules:

* Path Planning
* Coverage Analysis
* Weather Analysis


Aerial application consists of an aircraft spraying pesticide products on crops. The path planning module was built to create and execute a set of waypoints that define a flight mission for the simulated HiLStar17f aircraft. The program set waypoints to ensure uniform coverage of the field using either the back and forth pattern or the racetrack pattern. A weather module was constructed to determine if a given set of weather conditions were appropriate for aerial application.

The simulations were done using X-Plane 10 and a Pixhawk flight controller in a Hardware-in-loop configuration.

Here is a brief demonstration:
https://youtu.be/vwFg70kb8FQ

There is also a [paper](https://drive.google.com/file/d/0B_8MlvCvFrX9c0d1R3VhTFdNeU0/view) and a [presentation](https://docs.google.com/presentation/d/162M2WEAgpcKzQT05o-Kw_iIKF9eAGxBgKz76JJZROwM/pub?start=false&loop=false&delayms=3000).