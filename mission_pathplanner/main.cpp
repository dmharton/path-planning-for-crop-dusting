#include <iostream>
#include <fstream>
#include <list>
#include "json.hpp"
#include "field.h"
#include "LatLong-UTM.h"

// for convenience
using json = nlohmann::json;
using namespace std;
LatLongUTM latLongConverter;

//units in meters
const string COVERAGE_PATTERN = "BackAndForth"; // "Racetrack" or "BackAndForth"
const string FIELD_NAME = "Memorial"; // "Memorial" or "Soccer" or "Track"
const double SWATH_WIDTH = 7.0;
const double PASS_HEIGHT = 10.0 + 18;
const double PASS_SPEED_MS = 11.0;


struct coord {
    double x, y, height;
    double speed;
};

struct coordGPS {
    double lat, lon, height;
};


static double getDistance(coord c1, coord c2) {
    return sqrt(pow(c2.x - c1.x, 2) +
                pow(c2.y - c1.y, 2));
}

static double getDistance(point p1, point p2) {
    coord c1 = {p1.x, p1.y};
    coord c2 = {p2.x, p2.y};
    return getDistance(c1, c2);
}

static coord getCoordAlongLine(coord lineStart, coord lineEnd, double distance) {

    double lineDistance = getDistance(lineStart, lineEnd);
    double t = distance / lineDistance;

    coord result = {((1 - t) * lineStart.x) + (t * lineEnd.x),
                    ((1 - t) * lineStart.y) + (t * lineEnd.y)};

    return result;
}

static coord getCoordAlongLine(point lineStart, point lineEnd, double distance) {
    coord cStart = {lineStart.x, lineStart.y}
    , cEnd = {lineEnd.x, lineEnd.y};
    return getCoordAlongLine(cStart, cEnd, distance);
}


int main() {

    //// Retrieve mission file defining the field's edges

    Field field;
    string jsonString;
    field.loadFromMissionFile("/home/viki/ClionProjects/cropdusting/data/" + FIELD_NAME + "Field.mission", jsonString);

    json missionJson = json::parse(jsonString);

    //// Begin building path

    printf("f->height: %f, f->width: %f\n", field.height, field.width);
    string travelAxis = (field.height > field.width) ? "height" : "width";

    double travelAxisLength = (travelAxis == "height") ? field.height : field.width;
    double perpAxisLength = (travelAxis == "width") ? field.height : field.width;

    float passes, passesDecimal;

    passesDecimal = modf(perpAxisLength / SWATH_WIDTH, &passes);

    printf("passes = %f, passesDecimal = %f\n", passes, passesDecimal);

    double effectiveSwathWidth;

    if (passesDecimal <= 0.25) {
        effectiveSwathWidth = perpAxisLength / passes;
    } else if (passesDecimal >= 0.75) {
        effectiveSwathWidth = perpAxisLength / (passes + 1);
    } else {
        effectiveSwathWidth = SWATH_WIDTH;
        passes += 0.5;
    }

    printf("effectiveSwathWidth = %f\n", effectiveSwathWidth);
    printf("passes = %f\n", passes);


    array<point, 2> startLine, endLine;
    if (travelAxis == "height") {
        // start and end lines should be perpendicular to the travelAxis
        startLine = {field.points[0], field.points[1]};
        endLine = {field.points[3], field.points[2]};
    } else {
        startLine = {field.points[1], field.points[2]};
        endLine = {field.points[0], field.points[3]};
    }
    printf("startLine = (%f, %f), (%f, %f)\n", startLine[0].x, startLine[0].y, startLine[1].x, startLine[1].y);
    printf("endLine = (%f, %f), (%f, %f)\n", endLine[0].x, endLine[0].y, endLine[1].x, endLine[1].y);

    vector<coord> waypoints;
    float passesDone = 0;
    while (passesDone < passes) {

        if (passes - passesDone == 0.5) {
            //do half pass
        }

        coord passStart, passEnd;

        array<point, 2> passStartLine, passEndLine;

        if ((int) passesDone % 2 == 0) {
            passStartLine = startLine;
            passEndLine = endLine;
        } else { //return pass
            passStartLine = endLine;
            passEndLine = startLine;
        }


        if (COVERAGE_PATTERN == "BackAndForth") {

            passStart = getCoordAlongLine(passStartLine[0], passStartLine[1], (passesDone + 0.5) *
                                                                              effectiveSwathWidth); //add 0.5 because we want to start the pass 1/2 swaths into the field
            passStart.height = PASS_HEIGHT;
            passStart.speed = PASS_SPEED_MS;
            passEnd = getCoordAlongLine(passEndLine[0], passEndLine[1], (passesDone + 0.5) * effectiveSwathWidth);
            passEnd.height = PASS_HEIGHT;
            passEnd.speed = PASS_SPEED_MS;


            if (waypoints.empty()) { // add an extra waypoint to lead up
                coord firstWaypoint = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) + 200);
                firstWaypoint.height = PASS_HEIGHT + 30;
                waypoints.push_back(firstWaypoint);
            }


            waypoints.push_back(passStart);

            coord path1 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.75);
            path1.height = PASS_HEIGHT;
            waypoints.push_back(path1);
            coord path2 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.5);
            path2.height = PASS_HEIGHT;
            waypoints.push_back(path2);
            coord path3 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.25);
            path3.height = PASS_HEIGHT;
            waypoints.push_back(path3);

            waypoints.push_back(passEnd);

            //add turnaround
            if (passes - passesDone > 1) {
                //begin outward turn


                coord nextPassStart = getCoordAlongLine(passStartLine[0], passStartLine[1],
                                                        (passesDone + 1.5) * effectiveSwathWidth);
                coord nextPassEnd = getCoordAlongLine(passEndLine[0], passEndLine[1],
                                                      (passesDone + 1.5) * effectiveSwathWidth);

                double pass1pass2Distance = getDistance(passEnd, nextPassEnd);

                coord turnKeepStraight = getCoordAlongLine(passStart, passEnd, getDistance(passStart, passEnd) + 45);
                turnKeepStraight.height = PASS_HEIGHT + 12;
                turnKeepStraight.speed = PASS_SPEED_MS + 5;
                waypoints.push_back(turnKeepStraight);

                coord turnBegin = getCoordAlongLine(
                        getCoordAlongLine(nextPassStart, nextPassEnd, getDistance(nextPassStart, nextPassEnd) + 120),
                        getCoordAlongLine(passStart, passEnd, getDistance(passStart, passEnd) + 120),
                        pass1pass2Distance + 55
                );
                turnBegin.height = PASS_HEIGHT + 12;
                turnBegin.speed = PASS_SPEED_MS + 3;
                waypoints.push_back(turnBegin);

                coord turnEnd = getCoordAlongLine(nextPassStart, nextPassEnd,
                                                  getDistance(nextPassStart, nextPassEnd) + 140);
                turnEnd.height = PASS_HEIGHT + 5;
                turnEnd.speed = PASS_SPEED_MS;
                waypoints.push_back(turnEnd);

                coord turnEnd2 = getCoordAlongLine(nextPassStart, nextPassEnd,
                                                   getDistance(nextPassStart, nextPassEnd) + 60);
                turnEnd2.height = PASS_HEIGHT;
                turnEnd2.speed = PASS_SPEED_MS;
                waypoints.push_back(turnEnd2);
            }


        } else if (COVERAGE_PATTERN == "Racetrack") {

            double effPassesDone =
                    passesDone / 2 + 0.5; //add 0.5 because we want to start the pass 1/2 swaths into the field.
            if ((int) passesDone % 2 == 0) {

            } else { //return pass. need to move start over by 1/2 field
                effPassesDone += (passes - passesDecimal) / 2;
            }

            passStart = getCoordAlongLine(passStartLine[0], passStartLine[1], effPassesDone * effectiveSwathWidth);
            passStart.height = PASS_HEIGHT;
            passStart.speed = PASS_SPEED_MS;
            passEnd = getCoordAlongLine(passEndLine[0], passEndLine[1], effPassesDone * effectiveSwathWidth);
            passEnd.height = PASS_HEIGHT;
            passEnd.speed = PASS_SPEED_MS;


            if (waypoints.size() == 0) { // add an extra waypoint to lead up
                coord firstWaypoint = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) + 200);
                firstWaypoint.height = PASS_HEIGHT + 20;
                waypoints.push_back(firstWaypoint);
            }


            waypoints.push_back(passStart);

            coord path1 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.75);
            path1.height = PASS_HEIGHT;
            waypoints.push_back(path1);
            coord path2 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.5);
            path2.height = PASS_HEIGHT;
            waypoints.push_back(path2);
            coord path3 = getCoordAlongLine(passEnd, passStart, getDistance(passStart, passEnd) * 0.25);
            path3.height = PASS_HEIGHT;
            waypoints.push_back(path3);

            waypoints.push_back(passEnd);

            //add turnaround
            if (passes - passesDone > 1) {

                double nextEffPassesDone = (passesDone + 1) / 2 + 0.5;
                if ((int) passesDone % 2 == 0) {
                    nextEffPassesDone += (passes - passesDecimal) / 2;
                } else {
                    //nextEffPassesDone -= passes/2;
                }

                coord nextPassStart = getCoordAlongLine(passStartLine[0], passStartLine[1],
                                                        nextEffPassesDone * effectiveSwathWidth);
                coord nextPassEnd = getCoordAlongLine(passEndLine[0], passEndLine[1],
                                                      nextEffPassesDone * effectiveSwathWidth);

                double pass1pass2Distance = getDistance(passEnd, nextPassEnd);

                coord turnKeepStraight = getCoordAlongLine(passStart, passEnd, getDistance(passStart, passEnd) + 50);
                turnKeepStraight.height = PASS_HEIGHT + 12;
                turnKeepStraight.speed = PASS_SPEED_MS + 4;
                waypoints.push_back(turnKeepStraight);

                coord turnBegin = getCoordAlongLine(
                        getCoordAlongLine(nextPassStart, nextPassEnd, getDistance(nextPassStart, nextPassEnd) + 150),
                        getCoordAlongLine(passStart, passEnd, getDistance(passStart, passEnd) + 150),
                        pass1pass2Distance / 1
                );
                turnBegin.height = PASS_HEIGHT + 8;
                turnBegin.speed = PASS_SPEED_MS + 2;
                waypoints.push_back(turnBegin);

                coord turnEnd = getCoordAlongLine(nextPassStart, nextPassEnd,
                                                  getDistance(nextPassStart, nextPassEnd) + 70);
                turnEnd.height = PASS_HEIGHT;
                turnEnd.speed = PASS_SPEED_MS;
                waypoints.push_back(turnEnd);

                coord turnEnd2 = getCoordAlongLine(nextPassStart, nextPassEnd,
                                                   getDistance(nextPassStart, nextPassEnd) + 40);
                turnEnd2.height = PASS_HEIGHT;
                turnEnd2.speed = PASS_SPEED_MS;
                waypoints.push_back(turnEnd2);

            }


        }

        if (passes - passesDone < 1) { // add an extra waypoint to finish
            coord lastWaypoint = getCoordAlongLine(passStart, passEnd, getDistance(passStart, passEnd) + 100);
            lastWaypoint.height = PASS_HEIGHT;
            waypoints.push_back(lastWaypoint);
        }


        passesDone++;
        printf("END OF LOOP: passesDone = %f\n\n", passesDone);


    }

    ////Save waypoints into new mission file

    json itemTemplate;
    for (json &item : missionJson["items"]) {
        if (item["command"] == 16) {
            itemTemplate = item;
            break;
        }
    }

    missionJson["items"] = json::array();


    int i = 1;
    double prevSpeed = 0;

    for (auto &waypoint : waypoints) {
        json item = itemTemplate;

        double lat, lon;
        latLongConverter.UTMtoLL(eWGS84, waypoint.y, waypoint.x, field.utmZone, lat, lon);


        item["coordinate"][0] = lat;
        item["coordinate"][1] = lon;
        item["coordinate"][2] = waypoint.height;
        item["id"] = i++;

        missionJson["items"].push_back(item);


        if (waypoint.speed != 0 && waypoint.speed != prevSpeed) {

            item["id"] = i++;
            item["command"] = 178; //change speed type
            item["frame"] = 2;
            item["param1"] = 1;
            item["param2"] = waypoint.speed; //speed in meters per second
            item["param3"] = -1; //speed as a percentage of previous value
            missionJson["items"].push_back(item);

            prevSpeed = waypoint.speed;

        }

    }

    ofstream newMission(
            "/home/viki/ClionProjects/cropdusting/output/" + FIELD_NAME + "_" + COVERAGE_PATTERN + ".mission");
    if (!newMission.is_open()) {
        cout << "failed to create new file." << endl;
    }
    newMission << missionJson.dump();
    newMission.close();
    return 0;
}