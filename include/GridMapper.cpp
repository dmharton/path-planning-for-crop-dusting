//
// Created by viki on 11/3/16.
//

#include "opencv2/core/core.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std;

class GridMapper {
public:

    GridMapper(int width, int height) :
            canvas(height, width, CV_8UC3, cv::Scalar(0)),
            pathCanvas(height, width, CV_8UC3, cv::Scalar(0)){
        // Initialize random time generator
        srand(time(NULL));




        // Create resizeable named window
        cv::namedWindow("Occupancy Grid Canvas", \
      CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

        canvasMutex.lock();
        canvas = cv::Scalar(0);
        canvasMutex.unlock();
    };

    cv::Mat M, rotated, cropped;
    void getMeanStandardDeviationOfField(double centerX, double centerY,
                                         float width, float height, float angle,
                                         double& mean, double& stDev) {



        cv::Point2d center = toGridCoords({centerX, centerY});
        cv::Size2f size = {width + 3, height + 3};
        cv::RotatedRect rect( center, size, angle );
        // matrices we'll use
        //cv::Mat M, rotated, cropped;

        cv::Size2f rect_size = rect.size;
        if (rect.angle < -45.0) {
            angle += 90.0;
            std::swap(rect_size.width, rect_size.height);
        }
        // get the rotation matrix
        M = getRotationMatrix2D(rect.center, angle * -1.0, 1.0);
        // perform the affine transformation
        warpAffine(canvas, rotated, M, canvas.size(), cv::INTER_CUBIC);
        // crop the resulting image
        getRectSubPix(rotated, rect_size, rect.center, cropped);



        cv::Scalar arrMean, arrStDev;

        cv::meanStdDev(cropped, arrMean, arrStDev);

        mean = arrMean[0];
        stDev = arrStDev[0];

        cv::Point2f rectPoints[4];
        rect.points(rectPoints);

        for( int j = 0; j < 4; j++ ) {
            //cv::line( canvas, rectPoints[j], rectPoints[(j+1)%4], 255, 1, 8 );
        }

        //cv::imshow("Occupancy Grid Canvas", rotated);


    }

    double getDistance(cv::Point2d p1, cv::Point2d p2) {
        return sqrt(pow(p2.x - p1.x, 2) +
                    pow(p2.y - p1.y, 2));
    }

    double getDistance2(cv::Point2d p1, cv::Point2d p2) {
        // no sqrt
        return pow(p2.x - p1.x, 2) +
               pow(p2.y - p1.y, 2);
    }

    double dotProduct(cv::Point2d u, cv::Point2d v) {
        return u.x * v.x + u.y * v.y;
    }

    double getDistanceFromPointToLine(cv::Point2d lineStart, cv::Point2d lineEnd, cv::Point2d p) {

        // Return minimum distance between line segment vw and point p
        const double l2 = getDistance2(lineStart, lineEnd);  // i.e. |w-v|^2 -  avoid a sqrt
        if (l2 == 0.0) return getDistance(p, lineStart);   // v == w case
        // Consider the line extending the segment, parameterized as v + t (w - v).
        // We find projection of point p onto the line.
        // It falls where t = [(p-v) . (w-v)] / |w-v|^2
        // We clamp t from [0,1] to handle points outside the segment vw.
        const double t = std::max(0.0, std::min(1.0, dotProduct(p - lineStart, lineEnd - lineStart) / l2));
        const cv::Point2d projection = lineStart + t * (lineEnd - lineStart);  // Projection falls on the segment
        return getDistance(p, projection);


    }

    // Save a snapshot of the occupancy grid canvas
    void saveSnapshot(std::string filename) {
        using namespace boost::posix_time;

        //std::string filename = "/home/viki/ClionProjects/cropdusting/output/grid_" + to_iso_string(second_clock::local_time()) + ".png";
        canvasMutex.lock();
        cv::imwrite(filename, cropped);
        canvasMutex.unlock();
    };

    void plot(int x, int y, char value) {
        return plot(x, y, value, value, value);
    }

    // Update grayscale intensity on canvas pixel (x, y) (in robot coordinate frame)
    void plot(int x, int y, unsigned char B, unsigned char G, unsigned char R) {
        canvasMutex.lock();
        x = x + canvas.rows/2;
        y = -y + canvas.cols/2;
        if (x >= 0 && x < canvas.rows && y >= 0 && y < canvas.cols) {
            canvas.at<cv::Vec3b>(y, x) = cv::Vec3b(B, G, R);
            //canvas.at<cv::Vec3b>(y, x)[0] = B;
            //canvas.at<cv::Vec3b>(y, x)[1] = G;
            //canvas.at<cv::Vec3b>(y, x)[2] = R;
        }
        canvasMutex.unlock();
    };

    // Update grayscale intensity on canvas pixel (x, y) (in image coordinate frame)
    void plotImg(int x, int y, char value) {
        canvasMutex.lock();
        if (x >= 0 && x < canvas.cols && y >= 0 && y < canvas.rows) {
            canvas.at<char>(y, x) = value;
        }
        canvasMutex.unlock();
    };

    bool isCellEqualToStatus(int x, int y, char value) {
        x = x + canvas.rows/2;
        y = -y + canvas.cols/2;
        return canvas.at<char>(y, x) == value;
    }

    void plotLine(int x1, int y1, int x2, int y2, unsigned char B, unsigned char G, unsigned char R) {
        cv::line(canvas,
                 toGridCoords(cv::Point2d(x1, y1)),
                 toGridCoords(cv::Point2d(x2, y2)),
                 cv::Scalar(B, G, R));
    }


    //before I learned about cv::line()
    void plotLine(int x1, int y1, int x2, int y2, char color) {
        //Bresenham's line algorithm
        const bool isSteep = std::abs(y2 - y1) > std::abs(x2 - x1);
        if (isSteep) {
            std::swap(x1, y1);
            std::swap(x2, y2);
        }

        if (x1 > x2) {
            std::swap(x1, x2);
            std::swap(y1, y2);
        }

        int dX = x2 - x1;
        int dY = std::abs(y2 - y1);
        int err = dX/2;
        int yStep = (y1 < y2) ? 1 : -1;
        int y = y1;

        for (int x = x1; x <= x2; x++) {

            if (isSteep) {// && canvas.at<char>(y, x) == CELL_UNKNOWN) {
                plot(y, x, color);
            } else {// if (canvas.at<char>(x, y) == CELL_UNKNOWN) {
                plot(x, y, color);
            }

            err = err - dY;
            if (err < 0) {
                y += yStep;
                err += dX;
            }
        }

    };


    cv::Point2d toGridCoords(cv::Point2d p) {
        p.x = p.x + canvas.rows/2;
        p.y = -p.y + canvas.cols/2;
        return p;
    }

    void drawFlighPath(double startX, double startY, double endX, double endY, double swathWidth) {
        //draws the flight path between two points


        // A typical particle distribution is trapezoidal in shape. A flat distribution under the aircraft
        // and a linear reduction going toward the edge.
        // eg:    __+__ <- the plus is the aircraft
        //       /     \
        // It's reasonable to assume that the width of the angled portions are equal to the width of the
        // flat portion.
        // The swath width, in this case, would be the width of the flat portion plus 1/2 of the width
        // of the angled portions. This way the passes will overlap with each other for even coverage.

        cv::Point2d start(startX, startY),
                    end(endX, endY),
                    v;

        cv::Scalar c1(100,100,100),
                   c2(0,0,0);

        v = end - start;

        double mag = sqrt (v.x*v.x + v.y*v.y);
        v.x = v.x / mag;
        v.y = v.y / mag;

        double temp = v.x;
        v.x = v.y;
        v.y = temp;

        cv::Point2d flatStart(v * (swathWidth * 3.0/8.0));
        cv::Point2d flatEnd(v * -(swathWidth * 3.0/8.0));

        cv::Point2d vStart(v * (swathWidth * 5.0/8.0));
        cv::Point2d vEnd(v * -(swathWidth * 5.0/8.0));


        //cv::Mat blur(canvas.rows, canvas.cols, CV_8UC1);



        cv::LineIterator iter(canvas, toGridCoords({round(startX), round(startY)}), toGridCoords({round(endX), round(endY)}), 4);
        for (int i = 0; i < iter.count; i++, iter++) {

            cv::Point2d pos = {(double)iter.pos().x, (double)iter.pos().y};




            line2(pathCanvas, flatStart + pos, flatEnd + pos, c1, c1);
            line2(pathCanvas, flatStart + pos + v, vStart + pos, c1, c2);
            line2(pathCanvas, flatEnd + pos - v, vEnd + pos, c1, c2);


            for (int j=1; j< 10; j+=2) {
                //cv::GaussianBlur(blur, blur, cv::Size(j, j), 0);
                //cv::medianBlur(blur, blur, j);

            }

        }
        //canvas += blur;
        //blur = cv::Scalar(0,0,0);

    }
    void finishPass() {
        canvas += pathCanvas;
        pathCanvas = cv::Scalar(0,0,0);
    }

    void plotGradientAlongLine(int x1, int y1, int x2, int y2, double gx, double gy, char colorStart, char colorEnd) {
        //gx, gy represents a vector for the gradient to follow
        x1 = x1 + canvas.rows/2;
        x2 = x2 + canvas.rows/2;
        y1 = -y1 + canvas.cols/2;
        y2 = -y2 + canvas.cols/2;


        cv::Point start(x1, y1);
        cv::Point end(x2, y2);
        cv::Point g(gx, gy);

        cv::Scalar c1(colorStart,colorStart,colorStart);
        cv::Scalar c2(colorEnd,colorEnd,colorEnd);

        //start at i=1 to avoid overlapping
        cv::LineIterator iter(canvas, start, end);
        for (int i = 1; i < iter.count; i++, iter++) {

            line2(canvas, iter.pos(), iter.pos()+g, c1, c2);
        }

    }

    void plotGradient(int x1, int y1, int x2, int y2, char colorStart, char colorEnd) {
        x1 = x1 + canvas.rows/2;
        x2 = x2 + canvas.rows/2;
        y1 = -y1 + canvas.cols/2;
        y2 = -y2 + canvas.cols/2;


        cv::Point start(x1, y1);
        cv::Point end(x2, y2);
        cv::Scalar c1(colorStart,colorStart,colorStart);
        cv::Scalar c2(colorEnd,colorEnd,colorEnd);

        line2(canvas, start, end, c1, c2);
    }

    //http://www.rojtberg.net/929/how-to-draw-a-line-interpolating-2-colors-with-opencv/
    void line2(cv::Mat& img, const cv::Point& start, const cv::Point& end,
                             const cv::Scalar& c1,   const cv::Scalar& c2) {

       cv::LineIterator iter(img, start, end, 4);

        for (int i = 0; i < iter.count; i++, iter++) {
            double alpha = double(i) / iter.count;
            // note: using img.at<T>(iter.pos()) is faster, but
            // then you have to deal with mat type and channel number yourself

            //img(cv::Rect(iter.pos(), cv::Size(1, 1))) = c1 * (1.0 - alpha) + c2 * alpha;
            cv::Mat p = img(cv::Rect(iter.pos(), cv::Size(1, 1)));
            //auto p = img.at<char>(iter.pos());

            cv::Scalar brightness = (c1 * (1.0 - alpha) + c2 * alpha);

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotAssignable"
            img(cv::Rect(iter.pos(), cv::Size(1, 1))) = brightness;
#pragma clang diagnostic pop


        }
    }

    void show() {

        cv::imshow("Occupancy Grid Canvas", canvas);
    }

    const static int SPIN_RATE_HZ = 10;

    const static char CELL_OCCUPIED = 0;
    const static char CELL_UNKNOWN = 86;
    const static char CELL_FREE = 172;
    const static char CELL_ROBOT = 255;



protected:
    cv::Mat canvas; // Occupancy grid canvas
    cv::Mat pathCanvas; //stores individual passes before being applied to canvas
    boost::mutex canvasMutex; // Mutex for occupancy grid canvas object
};
