//
// Created by viki on 9/18/16.
//

#ifndef MISSION_PATHPLANNER_GPS_H
#define MISSION_PATHPLANNER_GPS_H


#include <array>

class gps {
public:
// This function converts decimal degrees to radians
    static long double deg2rad(long double deg);
//  This function converts radians to decimal degrees
    static long double rad2deg(long double rad);

/**
 * Returns the distance between two points on the Earth.
 * Direct translation from http://en.wikipedia.org/wiki/Haversine_formula
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in meters
 */
   static long double distanceEarth(long double lat1d, long double lon1d,  long double lat2d, long double lon2d);

   static std::array<long double, 2> getLatLonFromDistance(long double lat1d, long double lon1d, long double distX, long double distY);
};


#endif //MISSION_PATHPLANNER_GPS_H










