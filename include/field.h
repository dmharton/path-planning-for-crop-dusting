//
// Created by viki on 10/22/16.
//
using namespace std;

struct point {
    double x, y;
};


class Field {
private:

    point getVector(point p1, point p2);
    double getDistance(point p1, point p2);

public:
    vector<point> points;
    double width;
    double height;
    int utmZone;


    Field(double w=1, double h=1);

    Field(point p1, point p2, point p3, point p4);

    double getWidth();
    void setWidth(double w);

    double getHeight();
    void setHeight(double h);

    double getArea();
    double getPerimeter();

    void loadFromMissionFile(string file, string& contents);

    bool isPointInField(point p);

    point getCenter();
};