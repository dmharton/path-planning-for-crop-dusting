//
// Created by darren on 10/22/16.
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <tuple>
#include "json.hpp"
#include "field.h"
#include "LatLong-UTM.h"

using namespace std;
using json = nlohmann::json;


Field::Field(double w, double h) : width(w), height(h) {}

double Field::getArea() { return width * height; }

double Field::getPerimeter() { return 2 * (width + height); }

Field::Field(point p1, point p2, point p3, point p4) {
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
    points[3] = p4;

    //TODO: need to normalize whatever was entered into a perfect rectangle, but for now, I'll assume that's been done.

    width = getDistance(p1, p2);

    height = getDistance(p2, p3);

}


double Field::getDistance(point p1, point p2) {
    return sqrt(pow(p2.x - p1.x, 2) +
                pow(p2.y - p1.y, 2));
}

point Field::getVector(point p1, point p2) {
    return {
            p2.x - p1.x,
            p2.y - p1.y
    };
}

void Field::loadFromMissionFile(string file, string &fileContent) {
    ifstream ifs(file);
    string content((istreambuf_iterator<char>(ifs)),
                   (istreambuf_iterator<char>()));

    if (ifs.fail()) {
        cout << "Failed to load file!" << std::endl;
    }


    //This is the part where the coordinates are loaded in from the file.

    json field = json::parse(content);
    fileContent = content;

    //iterate through all of the items
    for (json &item : field["items"]) {


        if (item["command"] == 16) { // 16 means waypoint
            double coordX, coordY;

            struct {
                double lat, lon;
            } coords = {item["coordinate"][0], item["coordinate"][1]};

            LatLongUTM latLongConverter;
            latLongConverter.LLtoUTM(eWGS84, coords.lat, coords.lon, coordY, coordX, utmZone);

            points.push_back({coordX, coordY});
        }
    }

    width = getDistance(points[0], points[1]);
    height = getDistance(points[1], points[2]);
}

double dotProduct(point u, point v) {
    return u.x * v.x + u.y * v.y;
}

bool Field::isPointInField(point p) {


    point v01 = getVector(points[0], points[1]),
            v0p = getVector(points[0], p),
            v12 = getVector(points[1], points[2]),
            v1p = getVector(points[1], p);

    double dot010p = dotProduct(v01, v0p),
            dot0101 = dotProduct(v01, v01),
            dot121p = dotProduct(v12, v1p),
            dot1212 = dotProduct(v12, v12);

    return 0 <= dot010p && dot010p <= dot0101 &&
           0 <= dot121p && dot121p <= dot1212;

}

point Field::getCenter() {

    double sumX = 0, sumY = 0;

    for (point &p : points) {
        sumX += p.x;
        sumY += p.y;
    }
    return {sumX / points.size(), sumY / points.size()};
}

