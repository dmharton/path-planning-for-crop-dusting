//
// Created by darren on 9/18/16.
//

#include "gps.h"
#include <cmath>
#include <array>

#define earthRadiusM 6371000.0


// This function converts decimal degrees to radians
long double gps::deg2rad(long double deg) {
    return (deg * M_PI / 180);
}

//  This function converts radians to decimal degrees
long double gps::rad2deg(long double rad) {
    return (rad * 180 / M_PI);
}


long double gps::distanceEarth(long double lat1d, long double lon1d, long double lat2d, long double lon2d) {
    long double lat1r, lon1r, lat2r, lon2r, u, v;
    lat1r = deg2rad(lat1d);
    lon1r = deg2rad(lon1d);
    lat2r = deg2rad(lat2d);
    lon2r = deg2rad(lon2d);
    u = sin((lat2r - lat1r)/2);
    v = sin((lon2r - lon1r)/2);
    //return 2.0 * earthRadiusM * asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));// * 1000;
    long double a = u*u + cos(lat1r)*cos(lat2r) * v*v;
    long double c = 2 * atan2(sqrt(a), sqrt(1-a));
    return c * earthRadiusM;

}

std::array<long double,2> gps::getLatLonFromDistance(long double lat1d, long double lon1d, long double distX, long double distY) {
    long double lat1r, lon1r, lat2r, lon2r, r, theta, angdist;

    //distX = distX/1000;
    //distY = distY/1000;

    lat1r = deg2rad(lat1d);
    lon1r = deg2rad(lon1d);
    r = sqrt(distX*distX + distY*distY);
    theta = atan2(distX, distY); // http://www.igismap.com/formula-to-find-bearing-or-heading-angle-between-two-points-latitude-longitude/
    angdist = r/earthRadiusM;

    lat2r = asin( (sin(lat1r) * cos(angdist)) +
                  (cos(lat1r) * sin(angdist) * cos(theta)) );

    lon2r = lon1r + atan2( sin(theta) * sin(angdist) * cos(lat1r),
                           cos(angdist) * sin(lat1r) * sin(lat2r) );


    return {rad2deg(lat2r), rad2deg(lon2r)};
}